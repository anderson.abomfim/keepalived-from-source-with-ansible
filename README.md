# KeepAlived From Source with Ansible

This Ansible Playbook will automate the installation of the latest stable [KeepAlived](https://keepalived.org/) version - At the current time [2.2.7](https://keepalived.org/software/keepalived-2.2.7.tar.gz), from the source and create the respective configuration files based on the templates on a CentOS 7 target host.
The configuration template can be changed to match your needs for KeepAlived deployment. 

Please, make sure to run the playbook in an appropriate test environment first. 


## Tests

The current [Ansible Playbook](https://www.ansible.com/) was tested with [CentOS 7.8](https://wiki.centos.org/action/show/Manuals/ReleaseNotes/CentOS7.2009) and [Ansible 2.9.25](https://docs.ansible.com/ansible/2.9/roadmap/ROADMAP_2_9.html)

It is likely to work with other versions but has not yet been tested.

## Usage

Place the target host inside the `keepalived.ini` file and use the `keepalived.yml` Ansible Playbook to install KeepAlived 2.2.7 on the host.

```bash
ansible-playbook keepalived.yml
```

## If You Need a Different Version

Just updated the value of the variable `downloadlink` inside `group_vars/all.yml`. 

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.